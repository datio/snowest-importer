// Copyright 2018 SnoWest Magazine. All rights reserved.

package snowest

import (
	"database/sql"
	"errors"
	"fmt"
	"strings"
	"time"
)

func (o *Import) importUserUpgrades() error {
	doImport, ok := o.Config.Get("snowest.import_user_upgrades").(bool)
	if !ok {
		return errors.New("could not find value for 'snowest.import_user_upgrades' in config")
	}

	if !doImport {
		return nil
	}

	var (
		itemRows *sql.Rows
		refID    int

		paymentID     sql.NullInt64
		transactionID sql.NullString
		invoiceID     int
		userID        int
		productPeriod int
		invoiceDate   time.Time
		renewalType   string // either automatic or not ['manual', 'automatic', 'NA']
		productID     int
	)

	invalidInvoices := map[int]struct{}{}

	itemRows, err = vb.Txn.Query(`
		SELECT ref_id FROM bl_payments
		WHERE trans_type != 'PAYMENT'
	`)
	if err != nil {
		return err
	}

	for itemRows.Next() {
		err = itemRows.Scan(&refID)
		if err != nil {
			return err
		}

		invalidInvoices[refID] = struct{}{}
	}

	stmtActiveUpgradeInsert, err := xf2.Txn.Prepare(`
		INSERT INTO xf_user_upgrade_active
			(user_upgrade_record_id, datio_syncVbilling_is_vbilling, user_id, user_upgrade_id, extra, start_date, end_date)
		VALUES
			(?, 1, ?, ?, ?, ?, ?)
		ON DUPLICATE KEY UPDATE
			user_upgrade_record_id = VALUES(user_upgrade_record_id),
			user_id = VALUES(user_id),
			user_upgrade_id = VALUES(user_upgrade_id),
			extra = VALUES(extra),
			start_date = VALUES(start_date),
			end_date = VALUES(end_date)
	`)
	if err != nil {
		return err
	}

	stmtExpiredUpgradeInsert, err := xf2.Txn.Prepare(`
		INSERT INTO xf_user_upgrade_expired
			(user_upgrade_record_id, datio_syncVbilling_is_vbilling, user_id, user_upgrade_id, extra, start_date, end_date, original_end_date)
		VALUES
			(?, 1, ?, ?, ?, ?, ?, ?)
		ON DUPLICATE KEY UPDATE
			user_upgrade_record_id = VALUES(user_upgrade_record_id),
			user_id = VALUES(user_id),
			user_upgrade_id = VALUES(user_upgrade_id),
			extra = VALUES(extra),
			start_date = VALUES(start_date),
			end_date = VALUES(end_date),
			original_end_date = VALUES(original_end_date)
	`)
	if err != nil {
		return err
	}

	stmtUserGroupChangeInsert, err := xf2.Txn.Prepare(`
		INSERT INTO xf_user_group_change
			(user_id, change_key, group_ids)
		VALUES
			(?, ?, ?)
		ON DUPLICATE KEY UPDATE
			user_id = VALUES(user_id),
			change_key = VALUES(change_key),
			group_ids = VALUES(group_ids)
	`)
	if err != nil {
		return err
	}

	itemRows, err = vb.Txn.Query(`
		SELECT bl_payments.id as payment_id,
		       bl_payments.transaction_id,
		       bl_invoice.id as invoice_id,
			   bl_invoice.userid as user_id,
			   bl_invoice.product_period,
			   bl_invoice.stamp,
			   bl_invoice.product_renewal,
			   bl_invoice.product_id
		FROM bl_invoice
		LEFT JOIN bl_payments ON bl_payments.id = bl_invoice.payment_id
		WHERE bl_invoice.status = 1
		ORDER BY bl_invoice.id ASC
	`)
	if err != nil {
		return err
	}

	for itemRows.Next() {
		err = itemRows.Scan(&paymentID, &transactionID, &invoiceID, &userID, &productPeriod, &invoiceDate, &renewalType, &productID)
		if err != nil {
			return err
		}

		if _, exists := invalidInvoices[invoiceID]; exists {
			continue
		}

		if _, exists := userIDToUsernameMap[userID]; !exists {
			continue
		}

		var userUpgradeID int
		var endDate time.Time
		var groupIDs = []int{17} // Premium Membership group

		// Map to XF user upgrade IDs.
		switch productID {
		case 13:
			userUpgradeID = 1
			groupIDs = append(groupIDs, 30) // LIFETIME Membership group
		case 1:
			userUpgradeID = 3
			endDate = invoiceDate.AddDate(1, 0, 0)
		case 4:
			userUpgradeID = 2
			endDate = invoiceDate.AddDate(1, 0, 0)
		case 7, 10:
			userUpgradeID = 4
			endDate = invoiceDate.AddDate(2, 0, 0)
		case 11, 12:
			userUpgradeID = 5
			endDate = invoiceDate.AddDate(3, 0, 0)
		}

		extra := "{}"

		startDate := invoiceDate.Unix()

		if userUpgradeID == 1 || endDate.Sub(time.Now()) > 0 { // Not expired user upgrade
			_, err = stmtUserGroupChangeInsert.Exec(userID, fmt.Sprintf(
				"userUpgrade-%d",
				userUpgradeID), strings.Trim(strings.Join(strings.Fields(fmt.Sprint(groupIDs)), ","), "[]"),
			)
			if err != nil {
				return err
			}

			_, err = stmtActiveUpgradeInsert.Exec(invoiceID, userID, userUpgradeID, extra, startDate, endDate.Unix())
			if err != nil {
				return err
			}
		} else {
			_, err = stmtExpiredUpgradeInsert.Exec(invoiceID, userID, userUpgradeID, extra, startDate, endDate.Unix(), endDate.Unix())
			if err != nil {
				return err
			}
		}

	}

	return nil
}
