// Copyright 2018 SnoWest Magazine. All rights reserved.

package snowest

import "database/sql"

var userIDToUsernameMap = map[int]string{}

func (o *Import) prepareUserIDToUsernameMap() error {
	var (
		userRows *sql.Rows
		userID   int
		username string
	)

	userRows, err = xf2.Txn.Query(`
			SELECT user_id,
				   username
			FROM xf_user
		`)

	for userRows.Next() {
		err = userRows.Scan(&userID, &username)
		if err != nil {
			return err
		}

		userIDToUsernameMap[userID] = username
	}

	return nil
}
