// Copyright 2018 SnoWest Magazine. All rights reserved.

package main

import (
	"flag"
	"fmt"
	"os"

	toml "github.com/pelletier/go-toml"
	sw "gitlab.com/datio/snowest-importer"
)

type cli struct {
	ConfigFile string
}

var (
	importOptions *sw.Import
	cliOptions    *cli
	config        *toml.Tree
	err           error
)

func init() {
	importOptions = &sw.Import{}
	cliOptions = &cli{}

	flag.StringVar(&cliOptions.ConfigFile, "config", "", "configuration file")

	// Parse the above CLI arguments.
	flag.Parse()

	// Initialize the configuration.
	config, err = toml.LoadFile(cliOptions.ConfigFile)
	if err != nil {
		fmt.Println(sw.AddErrorInfo(err, "configuration error"))
		os.Exit(1)
	}
}

func main() {
	importOptions.Config = config
	err = importOptions.Begin()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
