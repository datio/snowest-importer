// Copyright 2018 SnoWest Magazine. All rights reserved.

package snowest

import (
	"io/ioutil"
	"os"
)

func (o *Import) migrate() error {
	err = o.prepareUserIDToUsernameMap()
	if err != nil {
		return err
	}

	// Import XFMG data.
	if err = o.importMedia(); err != nil { // method must include prepared statements.
		return err
	}

	if err = o.importPolls(); err != nil {
		return err
	}

	if err = o.importUserUpgrades(); err != nil {
		return err
	}

	return xf2.Txn.Commit()
}

func (o *Import) createEmptyDirIfNotExists(dirPath string, createEmptyHTLMFile bool) error {
	_, err = os.Stat(dirPath)
	if os.IsNotExist(err) {
		if err = os.MkdirAll(dirPath, 0755); err != nil {
			return err
		}
	} else {
		return nil
	}

	// XenForo index.html files include a space.
	if createEmptyHTLMFile {
		err = ioutil.WriteFile(dirPath+"index.html", []byte(" "), 0644)
	}

	return err
}
