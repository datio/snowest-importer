// Copyright 2018 SnoWest Magazine. All rights reserved.

package snowest

import (
	"database/sql"
	"errors"
	"fmt"
	"html"
	"strings"
)

func (o *Import) importPolls() error {
	doImport, ok := o.Config.Get("snowest.import_polls").(bool)
	if !ok {
		return errors.New("could not find value for 'snowest.import_polls' in config")
	}

	if !doImport {
		return nil
	}

	stmtPollInsert, err := xf2.Txn.Prepare(`
		INSERT INTO xf_poll
			(poll_id, content_type, content_id, question, responses, public_votes, max_votes, close_date)
		VALUES
			(?, 'thread', ?, ?, 'a:0:{}', ?, ?, ?)
		ON DUPLICATE KEY UPDATE
			content_type = VALUES(content_type),
			content_id = VALUES(content_id),
			question = VALUES(question),
			responses = VALUES(responses),
			public_votes = VALUES(public_votes),
			max_votes = VALUES(max_votes),
			close_date = VALUES(close_date)
	`)
	if err != nil {
		fmt.Println(err.Error())
		panic(err) // return err
	}

	stmtPollResponseInsert, err := xf2.Txn.Prepare(`
		INSERT INTO xf_poll_response
			(poll_id, response, voters)
		VALUES
			(?, ?, 'a:0:{}')
	`)
	if err != nil {
		fmt.Println(err.Error())
		panic(err) // return err
	}

	stmtPollVoteInsert, err := xf2.Txn.Prepare(`
		INSERT INTO xf_poll_vote
			(user_id, poll_response_id, poll_id, vote_date)
		VALUES
			(?, ?, ?, ?)
	`)
	if err != nil {
		fmt.Println(err.Error())
		panic(err) // return err
	}

	stmtThreadUpdate, err := xf2.Txn.Prepare(`
		UPDATE xf_thread
		SET discussion_type = 'poll'
		WHERE thread_id = ?
	`)
	if err != nil {
		fmt.Println(err.Error())
		panic(err) // return err
	}

	var (
		// polls
		itemRows  *sql.Rows
		pollID    int
		contentID int
		question  string
		date      int
		options   string
		timeout   int
		multiple  bool
		public    bool
	)

	itemRows, err = vb.Txn.Query(`
		SELECT poll.pollid, COALESCE(thread.threadid, 0), poll.question, poll.dateline, poll.options, poll.timeout, poll.multiple, poll.public
		FROM vb_poll poll
		LEFT JOIN vb_thread thread ON thread.pollid = poll.pollid
	`)
	if err != nil {
		fmt.Println(err.Error())
		panic(err) // return err
	}

	threadOptionToOptionIDMap := map[int][]int{}

	for itemRows.Next() {
		err = itemRows.Scan(&pollID, &contentID, &question, &date, &options, &timeout, &multiple, &public)
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}

		if contentID == 0 {
			continue
		}

		maxVotes := 0
		if multiple == false {
			maxVotes = 1
		}

		closeDate := 0
		if timeout > 0 {
			closeDate = date + timeout*86400
		}

		_, err = stmtPollInsert.Exec(pollID, contentID, html.UnescapeString(question), public, maxVotes, closeDate)
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}

		pollOptions := strings.Split(options, "|||")
		for _, pollOption := range pollOptions {
			res, err := stmtPollResponseInsert.Exec(pollID, pollOption)
			if err != nil {
				fmt.Println(err.Error())
				panic(err) // return err
			}

			id, _ := res.LastInsertId()
			threadOptionToOptionIDMap[pollID] = append(threadOptionToOptionIDMap[pollID], int(id))
		}

		_, err = stmtThreadUpdate.Exec(contentID)
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}
	}

	var (
		userID     int
		voteDate   int
		voteOption int
	)

	itemRows, err = vb.Txn.Query(`
		SELECT pollid, userid, votedate, voteoption
		FROM vb_pollvote
	`)
	if err != nil {
		fmt.Println(err.Error())
		panic(err) // return err
	}

	for itemRows.Next() {
		err = itemRows.Scan(&pollID, &userID, &voteDate, &voteOption)
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}

		pollResponseID, ok := threadOptionToOptionIDMap[pollID]
		if !ok {
			continue
		}

		if len(pollResponseID) < voteOption {
			continue
		}

		_, err = stmtPollVoteInsert.Exec(userID, pollResponseID[voteOption-1], pollID, voteDate)
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}
	}

	return nil
}
