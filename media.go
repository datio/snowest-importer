// Copyright 2018 SnoWest Magazine. All rights reserved.

package snowest

import (
	"crypto/md5"
	"database/sql"
	"errors"
	"fmt"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	"math"
	"os"
	"regexp"
	"strconv"

	"github.com/elgs/gostrgen"
	"github.com/oliamb/cutter"
	"github.com/rkravchik/resize"
)

// type vbGalleryAlbum struct {
// 	AlbumID         int
// 	UserID          int
// 	CreateDate      int
// 	LastPictureDate int
// 	Visible         bool
// 	Title           string
// 	Description     string
// 	CoverPictureId  int
// 	State           string
// 	Moderation      bool
// }

// type vbGalleryAlbumPicture struct {
// 	// vb_albumpicture
// 	AlbumID   int
// 	PictureID int
// 	Dateline  int
// 	// vb_picture
// 	UserID            int
// 	Caption           string
// 	Extension         string
// 	Filedata          string
// 	FileSize          string
// 	Width             int
// 	Height            int
// 	Thumbnail         string
// 	ThumbnailFileSize string
// 	ThumbnailWidth    int
// 	ThumbnailHeight   int
// 	ThumbnaiDateline  int
// 	IDHash            string
// 	ReportThreadID    int
// 	State             string
// }

var thumbnailOptionRe = regexp.MustCompile(`"width":([0-9]+).*"height":([0-9]+)`)

func (o *Import) importMedia() error {
	doImport, ok := o.Config.Get("snowest.import_media").(bool)
	if !ok {
		return errors.New("could not find value for 'snowest.import_media' in config")
	}

	if !doImport {
		return nil
	}

	albumCategoryID, ok := o.Config.Get("snowest.default_album_category_id").(int64)
	if !ok {
		return errors.New("could not find value for 'snowest.default_album_category_id' in config")
	}

	stmtAlbumInsert, err := xf2.Txn.Prepare(`
		INSERT INTO xf_mg_album
			(album_id, category_id, album_hash, title, description, create_date, last_update_date, view_privacy, add_users, user_id, username)
		VALUES
			(?, ?, ?, ?, ?, ?, ?, 'public', '[]', ?, ?)
		ON DUPLICATE KEY UPDATE
			category_id = VALUES(category_id),
			album_hash = VALUES(album_hash),
			title = VALUES(title),
			description = VALUES(description),
			create_date = VALUES(create_date),
			last_update_date = VALUES(last_update_date),
			view_privacy = VALUES(view_privacy),
			add_users = VALUES(add_users),
			user_id = VALUES(user_id),
			username = VALUES(username)
	`)
	if err != nil {
		fmt.Println(err.Error())
		panic(err) // return err
	}

	stmtAlbumWatch, err := xf2.Txn.Prepare(`
		INSERT INTO xf_mg_album_watch
			(user_id, album_id, notify_on, send_alert, send_email)
		VALUES
			(?, ?, 'media_comment', 1, 0)
		ON DUPLICATE KEY UPDATE
			notify_on = VALUES(notify_on),
			send_alert = VALUES(send_alert),
			send_email = VALUES(send_email)
	`)
	if err != nil {
		fmt.Println(err.Error())
		panic(err) // return err
	}

	stmtMediaItem, err := xf2.Txn.Prepare(`
		INSERT INTO xf_mg_media_item
			(media_id, media_hash, title, description, media_date, media_type, media_state, album_id, user_id, username, custom_fields, exif_data, tags)
		VALUES
			(?, ?, ?, '', ?, 'image', 'visible', ?, ?, ?, 'a:0:{}', '[]', 'a:0:{}')
		ON DUPLICATE KEY UPDATE
			media_hash = VALUES(media_hash),
			title = VALUES(title),
			description = VALUES(description),
			media_date = VALUES(media_date),
			media_type = VALUES(media_type),
			media_state = VALUES(media_state),
			album_id = VALUES(album_id),
			user_id = VALUES(user_id),
			username = VALUES(username),
			custom_fields = VALUES(custom_fields),
			exif_data = VALUES(exif_data),
			tags = VALUES(tags)
	`)
	if err != nil {
		fmt.Println(err.Error())
		panic(err) // return err
	}

	stmtMediaWatch, err := xf2.Txn.Prepare(`
		INSERT INTO xf_mg_media_watch
			(user_id, media_id, notify_on, send_alert, send_email)
		VALUES
			(?, ?, 'comment', 1, 0)
		ON DUPLICATE KEY UPDATE
			notify_on = VALUES(notify_on),
			send_alert = VALUES(send_alert),
			send_email = VALUES(send_email)
	`)
	if err != nil {
		fmt.Println(err.Error())
		panic(err) // return err
	}

	stmtAttachment, err := xf2.Txn.Prepare(`
		INSERT INTO xf_attachment
			(attachment_id, data_id, content_type, content_id, attach_date, unassociated)
		VALUES
			(?, ?, 'xfmg_media', ?, ?, 0)
		ON DUPLICATE KEY UPDATE
			data_id = VALUES(data_id),
			content_type = VALUES(content_type),
			content_id = VALUES(content_id),
			attach_date = VALUES(attach_date),
			unassociated = VALUES(unassociated)
	`)
	if err != nil {
		fmt.Println(err.Error())
		panic(err) // return err
	}

	// todo: Specifically check pseudo key: data_id-content_id for media items.
	stmtAttachmentData, err := xf2.Txn.Prepare(`
		INSERT INTO xf_attachment_data
			(data_id, user_id, upload_date, filename, file_size, file_hash, width, height, thumbnail_width, thumbnail_height, attach_count)
		VALUES
			(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1)
		ON DUPLICATE KEY UPDATE
			user_id = VALUES(user_id),
			upload_date = VALUES(upload_date),
			filename = VALUES(filename),
			file_size = VALUES(file_size),
			file_hash = VALUES(file_hash),
			width = VALUES(width),
			height = VALUES(height),
			thumbnail_width = VALUES(thumbnail_width),
			thumbnail_height = VALUES(thumbnail_height),
			attach_count = VALUES(attach_count)
	`)
	if err != nil {
		fmt.Println(err.Error())
		panic(err) // return err
	}

	var (
		// albums
		itemRows        *sql.Rows
		albumID         int
		userID          int
		createDate      int
		lastPictureDate int
		visible         bool
		title           string
		description     string
		coverPictureID  int
		state           string
		moderation      bool

		// media items
		mediaID int
	)

	xf2RootDir, ok := o.Config.Get("xf2.xenforo_directory").(string)
	if !ok {
		return errors.New("could not find value for 'xf2.xenforo_directory' in config")
	}

	vbGalleryDir, ok := o.Config.Get("vb3.gallery_dir").(string)
	if !ok {
		return errors.New("could not find value for 'vb3.gallery_dir' in config")
	}

	itemRows, err = vb.Txn.Query(`
		SELECT vb_albumpicture.pictureid, vb_picture.caption, vb_albumpicture.dateline, vb_albumpicture.albumid, userid FROM vb_albumpicture
		LEFT JOIN vb_picture ON vb_picture.pictureid = vb_albumpicture.pictureid
		ORDER BY vb_albumpicture.albumid DESC, vb_albumpicture.dateline
	`)
	if err != nil {
		fmt.Println(err.Error())
		panic(err) // return err
	}

	for itemRows.Next() {
		err = itemRows.Scan(&mediaID, &title, &createDate, &albumID, &userID)
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}

		var imageFile *os.File
		mediaIDSubDir := int(math.Floor(float64(mediaID) / 1000))
		sourceImagePath := fmt.Sprintf("%s%d/%d.picture", vbGalleryDir, mediaIDSubDir, mediaID)

		imageFile, err = os.OpenFile(sourceImagePath, os.O_RDONLY, 0644)
		if err != nil {
			continue
		}

		_, err = imageFile.Seek(0, 0)
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}

		var attachmentImage image.Image
		var format string

		attachmentImage, format, err = image.Decode(imageFile)
		if err != nil {
			_ = imageFile.Close()
			continue
		}

		var attachmentImageGIF *gif.GIF
		if format == "gif" {
			_, err = imageFile.Seek(0, 0)
			if err != nil {
				fmt.Println(err.Error())
				panic(err) // return err
			}

			attachmentImageGIF, err = gif.DecodeAll(imageFile)
		}

		_ = imageFile.Close()

		var dataID, attachmentID int64
		err = xf2.Txn.QueryRow(`
			SELECT attachment_id, data_id
			FROM xf_attachment
			WHERE content_type = 'xfmg_media'
			  AND content_id = ?`,
			mediaID).Scan(&dataID, &attachmentID)
		switch {
		case err == sql.ErrNoRows:
		case err != nil:
			return err
		}

		if dataID == 0 {
			res, err := stmtAttachmentData.Exec(nil, userID, createDate, title, 0, "", 0, 0, 0, 0)
			if err != nil {
				fmt.Println(err.Error())
				panic(err) // return err
			}
			dataID, _ = res.LastInsertId()
		}

		dataIDSubDir := int(math.Floor(float64(dataID) / 1000))

		err = o.createEmptyDirIfNotExists(fmt.Sprintf("%sinternal_data/attachments/%d/", xf2RootDir, dataIDSubDir), true)
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}

		tempPath := fmt.Sprintf("%sinternal_data/attachments/%d/%d_temp.data", xf2RootDir, dataIDSubDir, dataID)
		imageFile, err = os.Create(tempPath)
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}

		if format == "gif" {
			err = gif.EncodeAll(imageFile, attachmentImageGIF)
			if err != nil {
				err = jpeg.Encode(imageFile, attachmentImage, &jpeg.Options{Quality: 100})
			}
		} else {
			err = jpeg.Encode(imageFile, attachmentImage, &jpeg.Options{Quality: 100})
		}
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}

		// Calculate file hash.
		_, err = imageFile.Seek(0, 0)
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}
		h := md5.New()
		if _, err = io.Copy(h, imageFile); err != nil {
			_ = imageFile.Close()
			fmt.Println(err.Error())
			panic(err) // return err
		}
		mediaFileHash := fmt.Sprintf("%x", h.Sum(nil))

		var imageFileInfo os.FileInfo
		imageFileInfo, err = imageFile.Stat()
		imageFileSize := imageFileInfo.Size()

		_ = imageFile.Close()

		properPath := fmt.Sprintf("%sinternal_data/attachments/%d/%d-%s.data", xf2RootDir, dataIDSubDir, dataID, mediaFileHash)
		err = os.Rename(tempPath, properPath)
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}

		// Create media thumbnails.
		var serializedOptionValue string
		var thumbnailWidth, thumbnailHeight int
		err = xf2.Txn.QueryRow(`
			SELECT option_value FROM xf_option
			WHERE option_id = 'xfmgThumbnailDimensions'
		`).Scan(&serializedOptionValue)
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}

		optionItemsMatches := thumbnailOptionRe.FindStringSubmatch(serializedOptionValue)
		thumbnailWidth, err = strconv.Atoi(optionItemsMatches[1])
		if err != nil {
			return AddErrorInfo(err, "thumbnailWidth")
		}

		thumbnailHeight, err = strconv.Atoi(optionItemsMatches[2])
		if err != nil {
			return AddErrorInfo(err, "thumbnailHeight")
		}

		if thumbnailHeight == 0 || thumbnailWidth == 0 {
			thumbnailHeight = 300
			thumbnailWidth = 300
		}

		b := attachmentImage.Bounds()
		originalImageWidth := b.Max.X
		originalImageHeight := b.Max.Y

		if b.Max.X >= b.Max.Y {
			if float64(b.Max.Y)/float64(b.Max.X) < 0.001 {
				continue
			}
			attachmentImage = resize.Resize(0, uint(thumbnailHeight), attachmentImage, resize.Lanczos3)
		} else {
			if float64(b.Max.X)/float64(b.Max.Y) < 0.001 {
				continue
			}
			attachmentImage = resize.Resize(uint(thumbnailWidth), 0, attachmentImage, resize.Lanczos3)
		}

		attachmentImage, err = cutter.Crop(attachmentImage, cutter.Config{
			Width:  thumbnailWidth,
			Height: thumbnailHeight,
			Mode:   cutter.Centered,
		})
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}

		err = o.createEmptyDirIfNotExists(fmt.Sprintf("%sdata/xfmg/thumbnail/%d/", xf2RootDir, mediaIDSubDir), true)
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}

		imageFile, err = os.Create(fmt.Sprintf("%sdata/xfmg/thumbnail/%d/%d-%s.jpg", xf2RootDir, mediaIDSubDir, mediaID, mediaFileHash))
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}
		b = attachmentImage.Bounds()

		newThumbnailWidth := b.Max.X
		newThumbnailHeight := b.Max.Y

		_ = jpeg.Encode(imageFile, attachmentImage, &jpeg.Options{Quality: 100})
		_ = imageFile.Close()

		err = o.createEmptyDirIfNotExists(fmt.Sprintf("%sdata/attachments/%d/", xf2RootDir, dataIDSubDir), true)
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}

		imageFile, err = os.Create(fmt.Sprintf("%sdata/attachments/%d/%d-%s.jpg", xf2RootDir, dataIDSubDir, dataID, mediaFileHash))
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}

		_ = png.Encode(imageFile, attachmentImage)
		_ = imageFile.Close()

		_, err = stmtAttachment.Exec(attachmentID, dataID, mediaID, createDate)
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}

		_, err = stmtAttachmentData.Exec(dataID, userID, createDate, fmt.Sprintf("%s.%s", title, format), imageFileSize, mediaFileHash, originalImageWidth, originalImageHeight, newThumbnailWidth, newThumbnailHeight)
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}

		_, err = stmtMediaItem.Exec(mediaID, mediaFileHash, title, createDate, albumID, userID, userIDToUsernameMap[userID])
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}

		_, err = stmtMediaWatch.Exec(userID, mediaID)
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}
	}

	itemRows, err = vb.Txn.Query(`
		SELECT albumid, userid, createdate, lastpicturedate, IF(visible > 0, TRUE, FALSE), title, description, coverpictureid, state, moderation
		FROM vb_album
		ORDER BY albumid
	`)
	if err != nil {
		fmt.Println(err.Error())
		panic(err) // return err
	}

	for itemRows.Next() {
		r, err := gostrgen.RandGen(32, gostrgen.Lower|gostrgen.Digit, "", "ghijklmnopqrstuvwxyz")
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}

		err = itemRows.Scan(&albumID, &userID, &createDate, &lastPictureDate, &visible, &title, &description, &coverPictureID, &state, &moderation)
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}

		_, err = stmtAlbumInsert.Exec(albumID, albumCategoryID, r, title, description, createDate, lastPictureDate, userID, userIDToUsernameMap[userID])
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}

		_, err = stmtAlbumWatch.Exec(userID, albumID)
		if err != nil {
			fmt.Println(err.Error())
			panic(err) // return err
		}
	}

	return nil
}
